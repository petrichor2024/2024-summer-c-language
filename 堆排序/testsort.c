#define _CRT_SECURE_NO_WARNINGS 1
#include "Heap.h"
//void HPTest01()
//{
//	HP hp;
//	HPInit(&hp);
//	int arr1[6] = { 34,29,48,23,10,50 };
//	for (int i = 0; i < 6; i++)
//	{
//		HPPush(&hp, arr1[i]);
//	}
//	int i = 0;
//	while (!HPEmpty(&hp))
//	{
//		arr1[i++] = HPTop(&hp);
//		printf("%d ", HPTop(&hp));
//		HPPop(&hp);
//	}
//	HPDestroy(&hp);
//}
//int main()
//{
//	HPTest01();
//	return 0;
//}


//第一种借用已有的堆

//1、需要堆的数据结构
//2、空间复杂度0(N) 创建了堆，其中数组占n个
void HeapSort1(int* a, int n)
{
	HP hp;
	for (int i = 0; i < n; i++)
	{
		HPPush(&hp, a[i]);
	}
	int i = 0;
	while (!HPEmpty(&hp))
	{
		a[i++] = HPTop(&hp);
		HPPop(&hp);
	}
	HPDestroy(&hp);
}

//向上调整算法建堆
void HeapSort2(int* arr, int n)
{
	int i = 0;
	for ( i = 0; i < 6; i++)
	{
		HPAdjustUp(arr, i);
	}
	int end = n - 1;
	while (end)
	{
		Swap(&arr[0],&arr[end]);
		HPAdjustDown(arr, 0, end);
		end--;
	}
}

//向下调整算法建堆
void HeapSort3(int* arr, int n)
{
	for (int i =(n-1-1)/2 ; i >= 0; i--)
	{
		HPAdjustDown(arr, i, n);
	}
	int end = n - 1;
	while (end)
	{
		Swap(&arr[0], &arr[end]);
		HPAdjustDown(arr, 0, end);
		end--;
	}
}
int main()
{
	int arr[] = { 17,20,13,10,19,15 };
	//HeapSort1(arr, 6);
	//HeapSort2(arr, 6);
	HeapSort3(arr, 6);
	for (int i = 0; i < 6; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}












