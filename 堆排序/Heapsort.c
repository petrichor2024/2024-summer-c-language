#define _CRT_SECURE_NO_WARNINGS 1
#include "Heap.h"
void HPInit(HP* php)
{
	php->arr = NULL;
	php->capacity = php->size = 0;
}

void Swap(int* x, int* y)
{
	int tmp = *x;
	*x = *y;
	*y = tmp;
}
void HPAdjustUp(HPDataType* arr, int child)
{
	int parent = (child - 1) / 2;
	while (child > 0)
	{
		if (arr[child] < arr[parent])
		{
			Swap(&arr[child], &arr[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}

void HPPush(HP* php, HPDataType x)
{
	assert(php);
	if (php->capacity == php->size)
	{
		int newcapacity = php->capacity == 0 ? 4 : 2 * php->capacity;
		HPDataType* tmp = (HPDataType*)realloc(php->arr, newcapacity * sizeof(HPDataType));
		if (tmp == NULL)
		{
			perror("realloc fail!");
			exit(1);
		}
		php->arr = tmp;
		php->capacity = newcapacity;
	}
	php->arr[php->size] = x;
	HPAdjustUp(php->arr,php->size);
	++php->size;

}
 

void HPAdjustDown(HPDataType* arr, int parent,int size)
{
	int child = parent * 2 + 1;
	while (child < size)
	{
		if (child + 1 < size&& arr[child] > arr[child + 1])
			child++;
		if (arr[child] < arr[parent])
		{
			Swap(&arr[child], &arr[parent]);
			parent = child;
			child = 2 * parent + 1;
		}
		else
			break;
	}
}



void HPPop(HP* php)
{
	assert(php && php->size);
	Swap(&(php->arr[php->size - 1]), &(php->arr[0]));
	--php->size;
	HPAdjustDown(php->arr, 0,php->size);
}



bool HPEmpty(HP* php)
{
	return php->size == 0;
}

HPDataType HPTop(HP* php)
{
	return php->arr[0];
}

void HPDestroy(HP* php)
{
	if (php->arr)
		free(php->arr);
	php->arr = NULL;
	php->size = php->capacity = 0;
}