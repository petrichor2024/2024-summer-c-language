#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

void PrintArr(int* arr, int n);
void InsertSort(int* arr, int n);
void HeapSort(int* arr, int n);
void SelectSort1(int* arr, int n);
void SelectSort(int* arr, int n);
void ShellSort(int* arr, int n);