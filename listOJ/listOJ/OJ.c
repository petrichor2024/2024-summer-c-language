#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
//移除指定位置元素

  struct ListNode {
   int val;
    struct ListNode *next;
  };
 
typedef struct ListNode ListNode;
ListNode* removeElements(ListNode* head, int val) {
    ListNode* pcur = head;
    ListNode* newhead = NULL;
    ListNode* newtail = NULL;
    while (pcur) {
        if (pcur->val != val) {
            if (newhead == NULL) {
                newhead = newtail = pcur;
            }
            else {
                newtail->next = pcur;
                newtail = newtail->next;
            }
        }
        pcur = pcur->next;
    }
    if (newtail)
        newtail->next = NULL;
    return newhead;
}










//反转链表元素
//1.头插法
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
typedef struct ListNode ListNode;
struct ListNode* reverseList(struct ListNode* head) {
    ListNode* newhead, * newtail;
    newhead = newtail = NULL;
    ListNode* pcur = head;
    while (pcur) {
        if (newhead == NULL) {
            newhead = newtail = pcur;
            pcur = pcur->next;
            newtail->next = NULL;
        }
        else {

            ListNode* p1 = newhead;
            newhead = pcur;
            pcur = pcur->next;
            newhead->next = p1;
        }
    }
    return newhead;
}






//三指针法

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
typedef struct ListNode ListNode;
struct ListNode* reverseList(struct ListNode* head) {
    if (head == NULL)
        return head;
    ListNode* n1, * n2, * n3;
    n1 = NULL, n2 = head, n3 = head->next;
    while (n3) {
        n2->next = n1;
        n1 = n2;
        n2 = n3;
        n3 = n3->next;
    }
    n2->next = n1;
    return n2;

}









//链表的中间节点
//快慢指针法
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
typedef struct ListNode ListNode;
struct ListNode* middleNode(struct ListNode* head) {
    ListNode* slow, * fast;
    slow = fast = head;
    while (fast && fast->next)//不能交换顺序
    {
        slow = slow->next;
        fast = fast->next->next;
    }
    return slow;
}










//返回链表倒数第k个数
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */

typedef struct ListNode ListNode;
int kthToLast(struct ListNode* head, int k) {
    ListNode* n1, * n2, * n3;
    n1 = NULL;
    n2 = head;
    n3 = head->next;
    while (n2)
    {
        n2->next = n1;
        n1 = n2;
        n2 = n3;
        if (n3)
            n3 = n3->next;
    }
    while (--k)
    {
        n1 = n1->next;
    }
    return n1->val;
}







//合并两个有序链表
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
typedef struct ListNode ListNode;
struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2) {
    ListNode* newtail, * newhead;
    newhead = newtail = NULL;
    ListNode* p1, * p2;
    p1 = list1;
    p2 = list2;
    while (p1 && p2) {
        if (p1->val < p2->val) {
            if (newhead == NULL)
                newhead = newtail = p1;
            else {
                newtail->next = p1;
                newtail = newtail->next;
            }
            p1 = p1->next;
        }
        else {
            if (newhead == NULL)
                newhead = newtail = p2;
            else {
                newtail->next = p2;
                newtail = newtail->next;
            }
            p2 = p2->next;
        }
    }
    if (p1) {
        if (newtail)
            newtail->next = p1;
        else {
            newhead = p1;
        }
    }
    else {
        if (newtail)
            newtail->next = p2;
        else {
            newhead = p2;
        }
    }
    return newhead;
}

//代码冗余,因为创建了一个空链表,每次都要判断是否为空
//所以考虑直接创建一个非空链表
//即创建一个带有头结点的链表
//头结点不保存有效数据,最后返回newhead->next即可
//不要忘记释放动态内存的空间,释放前先保存newhead->next
// newtail=newhead = (ListNode*)malloc(sizeof(ListNode));














//现有一链表的头指针 ListNode* pHead，给一定值x，
// 编写一段代码将所有小于x的结点排在其余结点之前，且不能改变原来的数据顺序，
// 返回重新排列后的链表的头指针。




/*
struct ListNode {
    int val;
    struct ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};*/
typedef  struct ListNode ListNode ;
 ListNode* partition(ListNode* pHead, int x) {
        // write code here
        //创建两个非空链表,小链表和大链表
        ListNode* lesshead, *lesstail;
        ListNode* greathead, *greattail;
        lesshead = lesstail = (ListNode*)malloc(sizeof(ListNode));
        greathead = greattail = (ListNode*)malloc(sizeof(ListNode));
        ListNode* pcur = pHead;
        while (pcur) {
            if (pcur->val < x) {
                lesstail->next = pcur;
                lesstail = lesstail->next;
            } else {
                greattail->next = pcur;
                greattail = greattail->next;
            }
            pcur=pcur->next;
        }
        lesstail->next=greathead->next;
        ListNode* ret=lesshead->next;
        free(lesshead);
        free(greathead);
        lesshead=NULL;
        greattail=NULL;
        return ret;
    }





 /*
 struct ListNode {
     int val;
     struct ListNode *next;
     ListNode(int x) : val(x), next(NULL) {}
     };*/
 class Partition {
 public:
     ListNode* partition(ListNode* pHead, int x) {
         // write code here
         //创建两个非空链表,小链表和大链表
         ListNode* lesshead, * lesstail;
         ListNode* greathead, * greattail;
         lesshead = lesstail = (ListNode*)malloc(sizeof(ListNode));
         greathead = greattail = (ListNode*)malloc(sizeof(ListNode));
         ListNode* pcur = pHead;
         while (pcur) {
             if (pcur->val < x) {
                 lesstail->next = pcur;
                 lesstail = lesstail->next;
             }
             else {
                 greattail->next = pcur;
                 greattail = greattail->next;
             }
             pcur = pcur->next;
         }
         greattail->next = NULL;//不然死循环了!!重点


         //合并
         lesstail->next = greathead->next;
         ListNode* ret = lesshead->next;
         free(lesshead);
         free(greathead);
         lesshead = NULL;
         greattail = NULL;
         return ret;
     }
 };







 //回文链表



 /*
struct ListNode {
    int val;
    struct ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};*/
#include <cstddef>
 class PalindromeList {
 public:
     bool chkPalindrome(ListNode* A) {
         // write code here
         ListNode* fast, * slow;
         slow = fast = A;
         while (fast && fast->next) {
             slow = slow->next;
             fast = fast->next->next;
         }
         ListNode* n1, * n2, * n3;
         n1 = NULL;
         n2 = slow;
         n3 = slow->next;
         while (n2) {
             n2->next = n1;
             n1 = n2;
             n2 = n3;
             if (n3)
                 n3 = n3->next;
         }
         ListNode* left = A;
         while (n1) {
             if (n1->val != left->val)
                 return false;
             n1 = n1->next;
             left = left->next;
         }
         return true;

     }
 };





//相交链表
 /**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
 typedef struct ListNode ListNode;
 struct ListNode* getIntersectionNode(struct ListNode* headA,
     struct ListNode* headB) {
     ListNode* p1 = headA;
     ListNode* p2 = headB;
     int sizeA, sizeB;
     sizeA = sizeB = 0;
     while (p1->next) {
         sizeA++;
         p1 = p1->next;
     }
     while (p2->next) {
         sizeB++;
         p2 = p2->next;
     }
     if (p1 == p2) {
         int size = abs(sizeA - sizeB);
         if (sizeA < sizeB) {
             while (size--) {
                 headB = headB->next;
             }
         }
         else {
             while (size--) {
                 headA = headA->next;
             }
         }
         while (headA) {
             if (headA == headB)
                 return headA;
             headA = headA->next;
             headB = headB->next;
         }

     }
     return NULL;
 }










 //判断是否环形链表
 /**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
 typedef struct ListNode ListNode;

 bool hasCycle(struct ListNode* head) {
     ListNode* slow, * fast;
     slow = fast = head;
     while (fast && fast->next && fast->next->next)//快慢指针,只要保证一个指针快一个指针慢就行
         //为了方便慢指针为1步,快指针2两步.
     {
         slow = slow->next;
         fast = fast->next->next->next;
         if (fast == slow)
             return true;
     }
     return false;

 }



 //判断并找出找出环形链表入环节点
 /**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
 typedef struct ListNode ListNode;
 ListNode* FindNode(ListNode* head)
 {
     ListNode* fast, * slow;
     slow = fast = head;
     while (fast && fast->next)
     {
         slow = slow->next;
         fast = fast->next->next;
         if (slow == fast)
             return slow;
     }
     return NULL;
 }
 struct ListNode* detectCycle(struct ListNode* head) {
     ListNode* meet = FindNode(head);
     ListNode* pcur = head;
     while (meet && pcur)
     {
         if (meet == pcur)
             return meet;
         meet = meet->next;
         pcur = pcur->next;
     }
     return NULL;
 }






 //给你一个长度为 n 的链表，每个节点包含一个额外增加的随机指针 random ，
 // 该指针可以指向链表中的任何节点或空节点。

 //    构造这个链表的 深拷贝。 深拷贝应该正好由 n 个 全新 节点组成
 // ，其中每个新节点的值都设为其对应的原节点的值。
 // 新节点的 next 指针和
 //  random 指针也都应指向复制链表中的新节点，
 // 并使原链表和复制链表中的这些指针能够表示相同的链表状态。
 // 复制链表中的指针都不应指向原链表中的节点 。

 //    例如，如果原链表中有 X 和 Y 两个节点，其中 X.random-- > Y 。那么在复制链表中对应的两个节点 x 和 y ，同样有 x.random-- > y 。

 //    返回复制链表的头节点。

 //    用一个由 n 个节点组成的链表来表示输入 / 输出中的链表。
 // 每个节点用一个[val, random_index] 表示：

 //    val：一个表示 Node.val 的整数。
 //    random_index：随机指针指向的节点索引（范围从 0 到 n - 1）；
 // 如果不指向任何节点，则为  null 。
 //    你的代码 只 接受原链表的头节点 head 作为传入参数。



 /**
 * Definition for a Node.
 * struct Node {
 *     int val;
 *     struct Node *next;
 *     struct Node *random;
 * };
 */
 typedef struct Node Node;
 Node* BuyNode(int x)
 {
     Node* newnode = (Node*)malloc(sizeof(Node));
     newnode->val = x;
     newnode->next = newnode->random = NULL;
     return newnode;
 }
 void AddNode(Node* phead)
 {
     Node* pcur = phead;
     while (pcur)
     {
         Node* ret = pcur->next;
         Node* newnode = BuyNode(pcur->val);
         pcur->next = newnode;
         newnode->next = ret;
         pcur = ret;
     }
 }
 struct Node* copyRandomList(struct Node* head) {
     if (head == NULL)
         return NULL;
     AddNode(head);
     Node* copy, * pcur, * p1, * newhead, * newtail;
     pcur = head;
     newhead = newtail = pcur->next;
     p1 = pcur;
     while (pcur)
     {
         copy = pcur->next;
         if (pcur->random != NULL)
         {
             copy->random = pcur->random->next;
         }
         pcur = copy->next;
     }
     while (p1->next->next)
     {
         p1 = p1->next->next;
         newtail->next = p1->next;
         newtail = newtail->next;
     }
     return newhead;

 }