#define _CRT_SECURE_NO_WARNINGS 1
#include "Queue.h"
void QueueTest01()
{
	Q q;//创建一个队列
	QueueInit(&q);
	QueuePush(&q, 1);
	QueuePush(&q, 2);
	QueuePush(&q, 3);
	QueuePush(&q, 4);
	///
	printf("head:%d\n", QueueFront(&q));
	printf("tail:%d\n", QueueBack(&q));
	printf("size:%d\n", QueueSize(&q));
	QueuePop(&q);
	QueueDestroy(&q);
}

int main()
{
	QueueTest01();
	return 0;
}
