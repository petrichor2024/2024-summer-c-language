#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
typedef int QDataType;
typedef struct QueueNode//队列节点的结构,即单链表节点的结构
{
	QDataType data;
	struct QueueNode* next;
}QNode;
typedef struct Queue//队列的结构,定义指向队列头尾的指针,以及队列节点的个数
{
	QNode* phead;
	QNode* ptail;
	QDataType size;
}Q;

void QueueInit(Q*);
//入队列,队尾
void QueuePush(Q*, QDataType);
//出队列,队头
void QueuePop(Q*);

//队列判空
bool QueueEmpty(Q*);


//取队头数据
QDataType QueueFront(Q*);

//取队尾数据
QDataType QueueBack(Q*);


//队列有效元素个数
int QueueSize(Q*);


void QueueDestroy(Q*);