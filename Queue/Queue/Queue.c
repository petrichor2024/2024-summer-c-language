#define _CRT_SECURE_NO_WARNINGS 1
#include "Queue.h"
void QueueInit(Q* pq)
{
	assert(pq);
	pq->phead = pq->ptail = NULL;
	pq->size = 0;
}


QNode* BuyNode(QDataType x)
{
	QNode* newnode = (QNode*)malloc(sizeof(QNode));
	if (newnode == NULL)
	{
		perror("malloc fail!");
		exit(1);
	}
	newnode->data = x;
	newnode->next = NULL;
	return newnode;
}


void QueuePush(Q* pq, QDataType x)
{
	assert(pq);
	if (pq->phead == NULL)
		pq->phead = pq->ptail = BuyNode(x);
	else
	{
		pq->ptail->next = BuyNode(x);
		pq->ptail = pq->ptail->next;
	}
	pq->size++;
}



bool QueueEmpty(Q* pq)
{
	assert(pq);
	return pq->phead == NULL && pq->ptail == NULL;
}
void QueuePop(Q* pq)
{
	assert(pq);
	assert(!QueueEmpty(pq));

	//只有一个节点的情况,避免ptail变成野指针
	if (pq->ptail == pq->phead)
	{
		free(pq->phead);
		pq->phead = pq->ptail = NULL;
	}
	else
	{
		QNode* next = pq->phead->next;
		free(pq->phead);
		pq->phead = next;
	}
	pq->size--;
}



QDataType QueueFront(Q* pq)
{
	assert(pq);
	assert(!QueueEmpty(pq));
	return pq->phead->data;
}


QDataType QueueBack(Q* pq)
{
	assert(pq);
	assert(!QueueEmpty(pq));
	return pq->ptail->data;
}


int QueueSize(Q* pq)
{
	assert(pq);

	//不规范且时间复杂度O(n)
	//int size = 0;
	//QNode* pcur = pq->phead;
	//while (pcur)
	//{
	//	size++;
	//	pcur = pcur->next;
	//}
	//return size;


	return pq->size;

}



void QueueDestroy(Q* pq)
{
	assert(pq);
	assert(!QueueEmpty(pq));
	QNode* pcur = pq->phead;
	while (pcur)
	{
		QNode* next = pcur->next;
		free(pcur);
		pcur = next;
	}
	pq->phead = pq->ptail = NULL;
	pq->size = 0;
}