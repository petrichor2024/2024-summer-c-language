#pragma once
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include<stdbool.h>
typedef int STDataType;
typedef struct Stack
{
	STDataType* arr;
	STDataType capacity;
	STDataType top;
}ST;

//初始化和销毁
void STInit(ST*);
void STDestroy(ST*);


//栈顶---入数据,出数据
void StackPush(ST*, STDataType);
void StackPop(ST*);

//判空
bool StackEmpty(ST*);

//取栈顶元素
STDataType StackTop(ST*);


//获取栈中有效元素个数
int STSize(ST*);