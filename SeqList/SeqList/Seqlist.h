#pragma once
#include <stdio.h>
#include<stdlib.h>
#include <assert.h>

typedef int sldatatype;
typedef struct Seqlist
{
	sldatatype* arr;
	sldatatype size;
	sldatatype capacity;
} sl;
void slinit(sl*);//初始化
void sldestroy(sl*);//销毁
void slprint(sl*);//打印
void checkcapacity(sl*);//判断空间是否足够
void slpushback(sl*, sldatatype);//尾插
void slpushfront(sl*, sldatatype);//头插
void slpopfront(sl*);//头删
void slpopback(sl*);//尾删

//在指定位置插入和删除数据
void slinsert(sl*, sldatatype, int);
void slfrase(sl*, int);


//查找指定数据
 int slfind(sl*, sldatatype);
