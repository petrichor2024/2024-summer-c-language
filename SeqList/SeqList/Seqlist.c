#define _CRT_SECURE_NO_WARNINGS 1
#include "Seqlist.h"
void slinit(sl* ps)
{
	ps->arr = NULL;
	ps->capacity = ps->size = 0;
}




void sldestroy(sl*ps)
{
	assert(ps);
	if (ps->arr)
	{
		free(ps->arr);
		ps->arr = NULL;
	}
	ps->capacity = ps->size = 0;
}



void slprint(sl* ps)
{
	assert(ps);
	int i = 0;
	for (i = 0; i < ps->size; i++)
	{
		printf("%d ", ps->arr[i]);
	}
}



void slcheckcapacity(sl* ps)
{
	assert(ps);
	if (ps->capacity == ps->size)
	{
		//增容
		//若capacity为0，给个默认值，否者乘以2
		int newcapacity = ps->capacity == 0 ? 4 : 2 * ps->capacity;
		sldatatype* tmp = (sldatatype*)realloc(ps->arr, newcapacity * sizeof(sldatatype));
		if (tmp == NULL)
		{
			perror("realloc fail!");
			exit(1);
		}
		ps->arr = tmp;
		ps->capacity = newcapacity;
	}

}

void slpushback(sl* ps, sldatatype x)
{
	assert(ps);
	slcheckcapacity(ps);
	ps->arr[ps->size++] = x;
}

void slpushfront(sl* ps, sldatatype x)
{
	assert(ps);
	slcheckcapacity(ps);
	for(int i=ps->size;i>0;i--)
	{
		ps->arr[i] = ps->arr[i - 1];
	}
	ps->arr[0] = x;
	ps->size++;
}

void slpopback(sl* ps)
{
	assert(ps && ps->size);
	ps->size--;
	//ps->arr[ps->size] = 0;多余了，没有必要
}



void slpopfront(sl* ps)
{
	assert(ps && ps->size);
	for (int i = 0; i <ps->size-1 ; i++)
	{
		ps->arr[i] = ps->arr[i + 1];
	}
	ps->size--;
}





void slinsert(sl* ps, sldatatype x, int pos)
{
	assert(ps);
	assert(pos >= 0 && pos <= ps->size);
	slcheckcapacity(ps);
	for (int i = ps->size; i > pos; i--)
	{
		ps->arr[i] = ps->arr[i - 1];
	}
	ps->arr[pos] = x;
	ps->size++;
}

void slfrase(sl* ps, int pos)
{
	assert(ps);
	assert(pos >= 0 && pos < ps->size);
	//还有更多的限制如顺序表不能为空
	for (int i = pos; i <ps->size-1 ; i++)
	{
		ps->arr[i] = ps->arr[i + 1];
	}


}


int slfind(sl* ps, sldatatype x)
{
	assert(ps);
	int i = 0;
	int flag = 0;
	for (i = 0; i < ps->size; i++)
	{
		if(ps->arr[i]==x)
		{
			flag = 1;
			break;
		}
	}
	if (flag)
		return 1;
	else
	{
		return -1;
	}
}


