#define _CRT_SECURE_NO_WARNINGS 1
//猜数字，数字范围为1-100
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
void menu()
{
	printf("***********************\n");
	printf("******    1. play *****\n");
	printf("******    0. exit *****\n");
	printf("***********************\n");
}
void game()
{
	int count = 8;
	int a = rand() % 100 + 1;
	int guess = 0;
	while (count)
	{
	printf("你还有%d次机会\n", count);
	printf("请输入1-100之内的数字:");
	scanf("%d", &guess);
	if (guess > a)
		printf("猜大了\n");
	else if (guess < a)
		printf("猜小了\n");
	else
	{
		printf("猜对了\n");
		break;
	}

	count--;
}
	if (count == 0)
		printf("很遗憾你没有猜出来\n");

}
int main()
{
	srand((unsigned int)time(NULL));
	int input = 0;
	do
	{
		menu();
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("输入错误，请重新输入\n");
			break;
		}

	} while (input);
	return 0;
}


