﻿#include"sort.h"

// 测试排序的性能对⽐
void TestOP()
{
	srand((unsigned int)time(NULL));
	const int N = 100000; 
	int* a5 = (int*)malloc(sizeof(int) * N);
	int* a6 = (int*)malloc(sizeof(int) * N);
	int* a8 = (int*)malloc(sizeof(int) * N);

	for (int i = 0; i < N; ++i)
	{
		a5[i] = rand();
		a6[i] = a5[i];
		a8[i] = a5[i];
	}



	int begin5 = clock();
	QuickSort(a5, 0, N - 1);
	//QuickSortNonR(a5, 0, N - 1);
	int end5 = clock();

	int begin6 = clock();
	MergeSort(a6, N);
	int end6 = clock();

	int begin8 = clock();
	CountSort(a8, N);
	int end8 = clock();

	printf("QuickSort:%d\n", end5 - begin5);
	printf("MergeSort:%d\n", end6 - begin6);
	printf("CountSort:%d\n", end8 - begin8);

	free(a5);
	free(a6);
	free(a8);
}
int main()
{
	////int a[] = { 5, 3, 9, 6, 2, 4, 7, 1, 8 };
	//int a[] = {100,101,109,105,101,105 };
	////int a[] = { 5, 3, 9, 6, 2 };
	//int n = sizeof(a) / sizeof(int);
	//printf("排序前：");
	//PrintArr(a, n);

	////QuickSort(a, 0, n - 1);
	////QuickSortNonR(a, 0, n - 1);
	////MergeSort(a, n);
	//CountSort(a, n);

	//printf("排序后：");
	//PrintArr(a, n);

	   
	TestOP();

	return 0;
}