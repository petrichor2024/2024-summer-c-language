#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
typedef int SLTDataType;
typedef struct SListNode 
{
	SLTDataType data;
	struct SListNode* next;
}SLTNode;


void creatlist();
SLTNode* SLTBuyNode(SLTDataType x);

void printlist(SLTNode*);
//插入
void SLTPushBack(SLTNode**, SLTDataType);
void SLTPushFront(SLTNode**, SLTDataType);


//删除
void SLTPopBack(SLTNode**);
void SLTPopFront(SLTNode**);

//查找
SLTNode* SLTFind(SLTNode* phead, SLTDataType x);

//指定位置插入数据
void SLTInsert(SLTNode**, SLTDataType, SLTNode*);
void SLTInsertAfter( SLTDataType, SLTNode*);


//删除指定节点
void SLTErase(SLTNode**, SLTNode*);
void SLTEraseAfter(SLTNode*);

//销毁链表
void SListDestroy(SLTNode**);