#define _CRT_SECURE_NO_WARNINGS 1
#include "sllist.h"
void printlist(SLTNode* phead)
{
	
	SLTNode* p1 = phead;
	while (p1)
	{
		printf("%d ", p1->data);
		p1 = p1->next;
	}
}

SLTNode* SLTBuyNode(SLTDataType x)
{
	SLTNode* node = (SLTNode*)malloc(sizeof(SLTNode));
	if (node == NULL)
	{
		perror("malloc fail!");
		exit(1);
	}
	node->data = x;
	node->next = NULL;
	return node;
}

void SLTPushBack(SLTNode** pphead , SLTDataType x)
{
	//申请新节点
	assert(pphead);
	SLTNode* newnode = SLTBuyNode(x);
	if (*pphead==NULL)
	{
		*pphead = newnode;
	}
	else
	{
		SLTNode* ptail = *pphead;
		while (ptail->next)
		{
			ptail = ptail->next;
		}
		ptail->next = newnode;

	}
}



void SLTPushFront(SLTNode** pphead, SLTDataType x)
{
	assert(pphead);
	SLTNode* newnode = SLTBuyNode(x);
	newnode->next = *pphead;
	*pphead = newnode;
}






void SLTPopBack(SLTNode** pphead)
{
	assert(pphead && *pphead);
	SLTNode* ptail = *pphead;
	SLTNode* prev = NULL;
	while (ptail->next)
	{
		prev = ptail;
		ptail = ptail->next;
	}
	if (prev)//处理只有一个节点的情况
		prev->next = NULL;
	else
		*pphead = NULL;
	free(ptail);
	ptail = NULL;
}



void SLTPopFront(SLTNode** pphead)
{
	assert(pphead && *pphead);
	SLTNode* next = (*pphead)->next;
	free(*pphead);
	*pphead = next;
}


SLTNode* SLTFind(SLTNode* phead, SLTDataType x)
{
	assert(phead);
	SLTNode* pcur = phead;
	while (pcur)
	{
		if (pcur->data==x)
		{
			return pcur;
		}
		pcur = pcur->next;

	}
	return NULL;

}




void SLTInsert(SLTNode** pphead, SLTDataType x, SLTNode* pos)
{
	assert(pphead && pos);
	if (pos == *pphead)
	{
		SLTPushFront(pphead,x);
	}
	else
	{
		SLTNode* newnode = SLTBuyNode(x);
		SLTNode* prev = *pphead;
		while (prev->next != pos)
		{
			prev = prev->next;
		}
		newnode->next = pos;
		prev->next = newnode;
	}
}



void SLTInsertAfter( SLTDataType x, SLTNode* pos)
{
	assert(pos);
	SLTNode* newnode = SLTBuyNode(x);
	newnode->next = pos->next;
	pos->next = newnode;
}



void SLTErase(SLTNode** pphead, SLTNode* pos )
{
	assert(pos && pphead&&*pphead);
	if (pos == *pphead)
	{
		SLTPopFront(pphead);
	}
	else
	{
		SLTNode* prev = *pphead;
		while (prev->next != pos)
		{
			prev = prev->next;
		}
		prev->next = pos->next;
		free(pos);
		pos = NULL;
	}
}





void SLTEraseAfter( SLTNode* pos)
{
	assert(pos && pos->next);
	SLTNode* del = pos->next;
	pos->next = pos->next->next;
	free(del);
	del = NULL;	
}


void SListDestroy(SLTNode** pphead)
{
	assert(pphead && *pphead);
	SLTNode* pcur = *pphead;
	while (pcur)
	{
		SLTNode* p1 = pcur;
		pcur = pcur->next;
		free(p1);
		p1 = NULL;
	}
	*pphead = NULL;
}

