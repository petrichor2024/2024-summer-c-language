#define _CRT_SECURE_NO_WARNINGS 1
#include "List.h"
//打印链表
void Print(LTNode* phead)
{
	assert(phead);
	LTNode* pcur = phead->next;
	while (pcur != phead)
	{
		printf("%d ", pcur->data);
		pcur = pcur->next;
	}
}

//申请新节点
LTNode* LTBuyNode(LTDataType x)
{
	LTNode* newnode = (LTNode*)malloc(sizeof(LTNode));
	if (newnode == NULL)
	{
		perror("malloc fail!");
		exit(1);
	}
	newnode->data = x;
	newnode->next = newnode->prev = newnode;
	return newnode;
}


//初始化链表
void LTInit(LTNode** pphead)
{
	//创建头结点(哨兵位)
	*pphead = LTBuyNode(-1);
}


//尾插
void LTPushBack(LTNode* phead, LTDataType x)
{
	LTNode* newnode = LTBuyNode(x);
	newnode->next = phead;
	newnode->prev = phead->prev;
	phead->prev->next = newnode;
	phead->prev = newnode;
}

//头插
void LTPushFront(LTNode* phead, LTDataType x)
{
	LTNode* newnode = LTBuyNode(x);
	newnode->next = phead->next;
	newnode->prev = phead;
	phead->next->prev = newnode;
	phead->next = newnode;
}

//判空
bool LTEmpty(LTNode* phead)
{
	assert(phead);
	return phead->next == phead;
}


//尾删
void LTPopBack(LTNode* phead)
{
	assert(phead);
	assert(!LTEmpty(phead));
	LTNode* del = phead->prev;
	phead->prev = del->prev;
	del->prev->next = phead;
	free(del);
	del = NULL;

}


//头删
void LTPopFront(LTNode* phead)
{
	assert(phead);
	assert(!LTEmpty(phead));
	LTNode* del = phead->next;
	del->next->prev = phead;
	phead->next = del->next;
	free(del);
	del = NULL;
}




//查找
LTNode* LTFind(LTNode* phead, LTDataType x)
{
	assert(phead);
	LTNode* pcur = phead->next;
	while (pcur != phead)
	{
		if (pcur->data == x)
		{
			return pcur;
		}
		pcur = pcur->next;
	}
	return NULL;
}



//在指定位置之后插入数据
void LTInsert(LTNode* pos, LTDataType x)
{
	assert(pos);
	LTNode* newnode = LTBuyNode(x);
	newnode->prev = pos;
	newnode->next = pos->next;
	//pos->next = newnode;
	//newnode->next->prev = newnode;
	pos->next->prev = newnode;
	pos->next = newnode;
}


//在指定位置之前插入数据
void LTInsertBefore(LTNode* pos, LTDataType x)
{
	assert(pos);
	LTNode* newnode = LTBuyNode(x);
	newnode->next = pos;
	newnode->prev = pos->prev;
	pos->prev = newnode;
	newnode->prev->next = newnode;
}

//删除指定位置节点
void LTErase(LTNode* pos)
{
	LTNode* del = pos;
	del->prev->next = pos->next;
	del->next->prev = del->prev;
	free(pos);
	pos = NULL;
}


//销毁链表
void LTDestroy(LTNode** pphead)
{
	assert(pphead&&*pphead);
	LTNode* pcur, * next;
	pcur = (*pphead)->next;
	while (pcur != *pphead)
	{
		next = pcur->next;
		free(pcur);
		pcur = next;
	}
	//销毁头结点
	free(*pphead);
	*pphead = NULL;
	pcur = NULL;
}


void LTDestroy2(LTNode* phead)
{
	assert(phead);
	LTNode* pcur = phead->next;
	while (pcur != phead)
	{
		LTNode* next = pcur->next;
		free(pcur);
		pcur = next;
	}
	free(phead);
	phead=pcur = NULL;
}


LTNode* LTInit2()
{
	return LTBuyNode(-1);
}