#define _CRT_SECURE_NO_WARNINGS 1
#include"List.h"
void ListTest01()
{
	//LTNode* plist = NULL;
	//LTInit(&plist);//初始化,只有一个哨兵位,为空链表
	 
	
	//保持接口一致性使用一级指针
	LTNode* plist = LTInit2();


	//测验尾插
	LTPushBack(plist, 4);
	//LTPushBack(plist, 3);
	//LTPushBack(plist, 4);
	//LTPushBack(plist, 5);

	//测验头插
	LTPushFront(plist, 1);
	LTPushFront(plist, 2);
	LTPushFront(plist, 3);
	//LTNode* pos=LTFind(plist, 2);
	//if (pos == NULL)
//	printf("没有找到\n");
//else
//	printf("找到了\n");


	//在指定位置之后插入数据;
	//LTInsert(pos, 6);
	//在指定位置之前插入数据
	//LTInsertBefore(pos, 7);

	//删除指定位置数据
	//LTErase(pos);
	

	//测验尾删
	//LTPopBack(plist);
	//LTPopBack(plist);
	//LTPopBack(plist);
	//LTPopBack(plist);
	// 测验头删
	//LTPopFront(plist);
	//LTPopFront(plist);
	//LTPopFront(plist);
	//LTPopFront(plist);
	//LTPopFront(plist);
	 
	//LTDestroy(&plist);

	//保持接口一致性使用一级指针
	LTDestroy2(plist);//记得把plist置为NULL
	plist = NULL;
	Print(plist);
}
int main()
{
	ListTest01();
	return 0;
}


