#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
typedef int LTDataType;
typedef struct ListNode
{
	LTDataType data;
	struct ListNode* next;
	struct ListNode* prev;
}LTNode;

//打印链表
void Print(LTNode*);
//初始化
void LTInit(LTNode**);


//插入数据
void LTPushBack(LTNode*,LTDataType);
void LTPushFront(LTNode*,LTDataType);


//删除
//判空
bool LTEmpty(LTNode*);

void LTPopBack(LTNode*);
void LTPopFront(LTNode*);


//查找
LTNode* LTFind(LTNode* phead, LTDataType x);

//在指定位置之前或之后插入节点
void LTInsert(LTNode* pos, LTDataType x);
void LTInsertBefore(LTNode* pos, LTDataType x);


//删除指定位置的节点
void LTErase(LTNode* pos);

//销毁
void LTDestroy(LTNode**);

//为了保持接口的一致性,优化代码
//将初始化和销毁函数传递的参数统一为一级指针
void LTDestroy2(LTNode*);
LTNode* LTInit2();