#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdbool.h>


//给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。
//
//有效字符串需满足：
//
//左括号必须用相同类型的右括号闭合。
//左括号必须以正确的顺序闭合。
//每个右括号都有一个对应的相同类型的左括号



char pairs(char a) {
    if (a == '}')
        return '{';
    else if (a == ']')
        return '[';
    else
        return '(';
}

bool isValid(char* s) {
    char str[10001] = { 0 };
    int i = 0;
    int top = 0;
    int len = strlen(s);
    for (i = 0; i < len; i++) {
        if (s[i] == '(' || s[i] == '{' || s[i] == '[')
            str[top++] = s[i];
        else {
            if (top == 0)
                return false;
            if (str[top - 1] != pairs(s[i]))
                return false;
            top--;
        }
    }
    return top == 0;
}





//请你仅使用两个队列实现一个后入先出（LIFO）的栈，
//并支持普通栈的全部四种操作（push、top、pop 和 empty）。
//
//实现 MyStack 类：
//
//void push(int x) 将元素 x 压入栈顶。
//int pop() 移除并返回栈顶元素。
//int top() 返回栈顶元素。
//boolean empty() 如果栈是空的，返回 true ；否则，返回 false 。
//
//
//注意：
//
//你只能使用队列的标准操作 —— 也就是 push to back、peek / pop from front、size 和 is empty 这些操作。
//你所使用的语言也许不支持队列。 你可以使用 list （列表）
//或者 deque（双端队列）来模拟一个队列, 只要是标准的队列操作即可。




#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
typedef int QDataType;
typedef struct QueueNode // 队列节点的结构,即单链表节点的结构
{
    QDataType data;
    struct QueueNode* next;
} QNode;
typedef struct Queue // 队列的结构,定义指向队列头尾的指针,以及队列节点的个数
{
    QNode* phead;
    QNode* ptail;
    QDataType size;
} Q;

void QueueInit(Q*);
// 入队列,队尾
void QueuePush(Q*, QDataType);
// 出队列,队头
void QueuePop(Q*);

// 队列判空
bool QueueEmpty(Q*);

// 取队头数据
QDataType QueueFront(Q*);

// 取队尾数据
QDataType QueueBack(Q*);

// 队列有效元素个数
int QueueSize(Q*);

void QueueDestroy(Q*);

void QueueInit(Q* pq) {
    assert(pq);
    pq->phead = pq->ptail = NULL;
    pq->size = 0;
}

QNode* BuyNode(QDataType x) {
    QNode* newnode = (QNode*)malloc(sizeof(QNode));
    if (newnode == NULL) {
        perror("malloc fail!");
        exit(1);
    }
    newnode->data = x;
    newnode->next = NULL;
    return newnode;
}

void QueuePush(Q* pq, QDataType x) {
    assert(pq);
    if (pq->phead == NULL)
        pq->phead = pq->ptail = BuyNode(x);
    else {
        pq->ptail->next = BuyNode(x);
        pq->ptail = pq->ptail->next;
    }
    pq->size++;
}

bool QueueEmpty(Q* pq) {
    assert(pq);
    return pq->phead == NULL && pq->ptail == NULL;
}
void QueuePop(Q* pq) {
    assert(pq);
    assert(!QueueEmpty(pq));

    // 只有一个节点的情况,避免ptail变成野指针
    if (pq->ptail == pq->phead) {
        free(pq->phead);
        pq->phead = pq->ptail = NULL;
    }
    else {
        QNode* next = pq->phead->next;
        free(pq->phead);
        pq->phead = next;
    }
    pq->size--;
}

QDataType QueueFront(Q* pq) {
    assert(pq);
    assert(!QueueEmpty(pq));
    return pq->phead->data;
}

QDataType QueueBack(Q* pq) {
    assert(pq);
    assert(!QueueEmpty(pq));
    return pq->ptail->data;
}

int QueueSize(Q* pq) {
    assert(pq);

    // 不规范且时间复杂度O(n)
    // int size = 0;
    // QNode* pcur = pq->phead;
    // while (pcur)
    //{
    //	size++;
    //	pcur = pcur->next;
    // }
    // return size;

    return pq->size;
}

void QueueDestroy(Q* pq) {
    assert(pq);
    // assert(!QueueEmpty(pq));
    QNode* pcur = pq->phead;
    while (pcur) {
        QNode* next = pcur->next;
        free(pcur);
        pcur = next;
    }
    pq->phead = pq->ptail = NULL;
    pq->size = 0;
}

typedef struct {
    Q q1;
    Q q2;
} MyStack;

MyStack* myStackCreate() {
    MyStack* pst = (MyStack*)malloc(sizeof(MyStack));
    QueueInit(&pst->q1);
    QueueInit(&pst->q2);
    return pst;
}

void myStackPush(MyStack* obj, int x) {
    if (!QueueEmpty(&obj->q1)) {
        QueuePush(&obj->q1, x);
    }
    else {
        QueuePush(&obj->q2, x);
    }
}

int myStackPop(MyStack* obj) {
    Q* empQ = &obj->q1;
    Q* noneQ = &obj->q2;
    if (!QueueEmpty(empQ)) {
        noneQ = &obj->q1;
        empQ = &obj->q2;
    }
    while (QueueSize(noneQ) > 1) {
        int front = QueueFront(noneQ);
        QueuePush(empQ, front);
        QueuePop(noneQ);
    }
    int pop = QueueFront(noneQ);
    QueuePop(noneQ);
    return pop;
}

int myStackTop(MyStack* obj) {
    Q* empQ = &obj->q1;
    Q* noneQ = &obj->q2;
    if (!QueueEmpty(empQ)) {
        noneQ = &obj->q1;
        empQ = &obj->q2;
    }
    while (QueueSize(noneQ) > 1) {
        int front = QueueFront(noneQ);
        QueuePush(empQ, front);
        QueuePop(noneQ);
    }
    int top = QueueFront(noneQ);
    QueuePush(empQ, top);
    QueuePop(noneQ);
    return top;
}

bool myStackEmpty(MyStack* obj) {
    return QueueEmpty(&obj->q1) && QueueEmpty(&obj->q2);
}

void myStackFree(MyStack* obj) {
    QueueDestroy(&obj->q1);
    QueueDestroy(&obj->q2);
    free(obj);
    obj = NULL;
}

/**
 * Your MyStack struct will be instantiated and called as such:
 * MyStack* obj = myStackCreate();
 * myStackPush(obj, x);

 * int param_2 = myStackPop(obj);

 * int param_3 = myStackTop(obj);

 * bool param_4 = myStackEmpty(obj);

 * myStackFree(obj);
*/





//请你仅使用两个栈实现先入先出队列。队列应当支持一般队列支持的所有操作（push、pop、peek、empty）：
//
//实现 MyQueue 类：
//
//void push(int x) 将元素 x 推到队列的末尾
//int pop() 从队列的开头移除并返回元素
//int peek() 返回队列开头的元素
//boolean empty() 如果队列为空，返回 true ；否则，返回 false
//说明：
//
//你 只能 使用标准的栈操作 —— 也就是只有 push to top, peek / pop from top, size, 和 is empty 操作是合法的。
//你所使用的语言也许不支持栈。你可以使用 list 或者 deque（双端队列）来模拟一个栈，只要是标准的栈操作即可。
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include<stdbool.h>
typedef int STDataType;
typedef struct Stack
{
    STDataType* arr;
    STDataType capacity;
    STDataType top;
}ST;

//初始化和销毁
void STInit(ST*);
void STDestroy(ST*);


//栈顶---入数据,出数据
void StackPush(ST*, STDataType);
void StackPop(ST*);

//判空
bool StackEmpty(ST*);

//取栈顶元素
STDataType StackTop(ST*);


//获取栈中有效元素个数
int STSize(ST*);

void STInit(ST* ps)
{
    assert(ps);
    ps->arr = NULL;
    ps->capacity = ps->top = 0;
}

void STDestroy(ST* ps)
{
    assert(ps);
    if (ps->arr)
        free(ps->arr);
    ps->arr = NULL;
    ps->top = ps->capacity = 0;
}

void StackPush(ST* ps, STDataType x)
{
    assert(ps);

    //判断空间是否足够
    if (ps->top == ps->capacity)
    {
        int newcapacity = ps->capacity == 0 ? 4 : 2 * ps->capacity;
        STDataType* tmp = (STDataType*)realloc(ps->arr, newcapacity * sizeof(STDataType));
        if (tmp == NULL)
        {
            perror("realloc fail!");
            exit(1);
        }
        ps->arr = tmp;
        ps->capacity = 2 * newcapacity;
    }
    ps->arr[ps->top++] = x;
}


bool StackEmpty(ST* ps)
{
    assert(ps);
    return ps->top == 0;
}
void StackPop(ST* ps)
{
    assert(ps);
    assert(!StackEmpty(ps));
    --ps->top;
}



STDataType StackTop(ST* ps)
{
    assert(ps);
    assert(!StackEmpty(ps));
    return ps->arr[ps->top - 1];
}

int STSize(ST* ps)
{
    return ps->top;
}





typedef struct {
    ST pushST;
    ST popST;
} MyQueue;


MyQueue* myQueueCreate() {
    MyQueue* pst = (MyQueue*)malloc(sizeof(MyQueue));
    STInit(&pst->pushST);
    STInit(&pst->popST);
    return pst;
}

void myQueuePush(MyQueue* obj, int x) {
    StackPush(&obj->pushST, x);
}

int myQueuePop(MyQueue* obj) {
    if (StackEmpty(&obj->popST))
    {
        while (!StackEmpty(&obj->pushST))
        {
            int top = StackTop(&obj->pushST);
            StackPush(&obj->popST, top);
            StackPop(&obj->pushST);
        }
    }
    int top = StackTop(&obj->popST);
    StackPop(&obj->popST);
    return top;

}

int myQueuePeek(MyQueue* obj) {
    if (StackEmpty(&obj->popST))
    {
        while (!StackEmpty(&obj->pushST))
        {
            int top = StackTop(&obj->pushST);
            StackPush(&obj->popST, top);
            StackPop(&obj->pushST);
        }
    }
    int top = StackTop(&obj->popST);
    return top;
}

bool myQueueEmpty(MyQueue* obj) {
    return StackEmpty(&obj->pushST) && StackEmpty(&obj->popST);
}

void myQueueFree(MyQueue* obj) {
    STDestroy(&obj->popST);
    STDestroy(&obj->pushST);
    free(obj);
    obj = NULL;
}

/**
 * Your MyQueue struct will be instantiated and called as such:
 * MyQueue* obj = myQueueCreate();
 * myQueuePush(obj, x);

 * int param_2 = myQueuePop(obj);

 * int param_3 = myQueuePeek(obj);

 * bool param_4 = myQueueEmpty(obj);

 * myQueueFree(obj);
*/













//设计你的循环队列实现。 循环队列是一种线性数据结构，其操作表现基于 FIFO（先进先出）原则并且队尾被连接在队首之后以形成一个循环。它也被称为“环形缓冲器”。
//
//循环队列的一个好处是我们可以利用这个队列之前用过的空间。在一个普通队列里，一旦一个队列满了，我们就不能插入下一个元素，即使在队列前面仍有空间。但是使用循环队列，我们能使用这些空间去存储新的值。
//
//你的实现应该支持如下操作：
//
//MyCircularQueue(k) : 构造器，设置队列长度为 k 。
//Front : 从队首获取元素。如果队列为空，返回 - 1 。
//Rear : 获取队尾元素。如果队列为空，返回 - 1 。
//enQueue(value) : 向循环队列插入一个元素。如果成功插入则返回真。
//deQueue() : 从循环队列中删除一个元素。如果成功删除则返回真。
//isEmpty() : 检查循环队列是否为空。
//isFull() : 检查循环队列是否已满






typedef struct {
    int* arr;
    int front;
    int rear;
    int capacity;
} MyCircularQueue;

MyCircularQueue* myCircularQueueCreate(int k) {
    MyCircularQueue* pst = (MyCircularQueue*)malloc(sizeof(MyCircularQueue));
    pst->arr = (int*)malloc(sizeof(int) * (k + 1));
    pst->front = pst->rear = 0;
    pst->capacity = k;
    return pst;
}

bool myCircularQueueIsFull(MyCircularQueue* obj) {
    return (obj->rear + 1) % (obj->capacity + 1) == obj->front;
}

bool myCircularQueueEnQueue(MyCircularQueue* obj, int value) {
    if (myCircularQueueIsFull(obj))
        return false;
    else {
        obj->arr[obj->rear++] = value;
    }
    obj->rear %= obj->capacity + 1;
    return true;
}

bool myCircularQueueIsEmpty(MyCircularQueue* obj) {
    return obj->rear == obj->front;
}
bool myCircularQueueDeQueue(MyCircularQueue* obj) {
    if (myCircularQueueIsEmpty(obj))
        return false;
    else {
        obj->front = (++obj->front) % (obj->capacity + 1);
    }
    return true;
}

int myCircularQueueFront(MyCircularQueue* obj) {
    if (myCircularQueueIsEmpty(obj))
        return -1;
    return obj->arr[obj->front];
}

int myCircularQueueRear(MyCircularQueue* obj) {
    if (myCircularQueueIsEmpty(obj))
        return -1;
    int tmp = obj->rear - 1;
    if (tmp == -1) {
        return obj->arr[obj->rear + obj->capacity];
    }
    else
        return obj->arr[tmp];
}

void myCircularQueueFree(MyCircularQueue* obj) {
    free(obj->arr);
    free(obj);
    obj = NULL;
}

/**
 * Your MyCircularQueue struct will be instantiated and called as such:
 * MyCircularQueue* obj = myCircularQueueCreate(k);
 * bool param_1 = myCircularQueueEnQueue(obj, value);

 * bool param_2 = myCircularQueueDeQueue(obj);

 * int param_3 = myCircularQueueFront(obj);

 * int param_4 = myCircularQueueRear(obj);

 * bool param_5 = myCircularQueueIsEmpty(obj);

 * bool param_6 = myCircularQueueIsFull(obj);

 * myCircularQueueFree(obj);
*