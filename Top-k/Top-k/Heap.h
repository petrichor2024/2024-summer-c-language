#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <time.h>

//С����
typedef int HPDataType;
typedef struct
{
	HPDataType* arr;
	HPDataType size;
	HPDataType capacity;
}HP;

//�ѵĳ�ʼ��
void HPInit(HP*);

//���ѵײ���Ԫ��
void Swap(int* x, int* y);
void HPPush(HP*, HPDataType );
void HPAdjustUp(HPDataType* arr,int child);

//ɾ���Ѷ�Ԫ��
void HPPop(HP*);
void HPAdjustDown(HPDataType* arr,int  parent,int size);

//�п�
bool HPEmpty(HP* php);
//ȡ�Ѷ�Ԫ��
HPDataType HPTop(HP* php);

//�ѵ�����
void HPDestroy(HP*);


